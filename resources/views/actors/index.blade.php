<div>
    <x-page-header>
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Actors') }}
            </h2>
            <div class="flex rounded-md shadow-sm">
                <div class="relative flex-grow focus-within:z-10">
                    <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                        <svg class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor"
                             aria-hidden="true">
                            <path fill-rule="evenodd"
                                  d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z"
                                  clip-rule="evenodd"/>
                        </svg>
                    </div>
                    <input type="text" wire:model.live.debounce.500ms="query"
                           class="hidden w-full rounded-none rounded-l-md border-0 py-1.5 pl-10 text-sm leading-6 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:block"
                           placeholder="Search actors">
                </div>

                <div class="relative" x-data="{ open: false }">
                    <button type="button"
                            class="relative w-full cursor-default rounded-r-md bg-white py-1.5 pl-3 pr-10 text-left text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-600 sm:text-sm sm:leading-6 flex"
                            aria-haspopup="listbox" aria-expanded="true" aria-labelledby="listbox-label"
                            @click="open = ! open">
                        <span class="pointer-events-none">
                        <svg class="-ml-0.5 h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor"
                             aria-hidden="true">
                            <path fill-rule="evenodd"
                                  d="M2 3.75A.75.75 0 012.75 3h11.5a.75.75 0 010 1.5H2.75A.75.75 0 012 3.75zM2 7.5a.75.75 0 01.75-.75h6.365a.75.75 0 010 1.5H2.75A.75.75 0 012 7.5zM14 7a.75.75 0 01.55.24l3.25 3.5a.75.75 0 11-1.1 1.02l-1.95-2.1v6.59a.75.75 0 01-1.5 0V9.66l-1.95 2.1a.75.75 0 11-1.1-1.02l3.25-3.5A.75.75 0 0114 7zM2 11.25a.75.75 0 01.75-.75H7A.75.75 0 017 12H2.75a.75.75 0 01-.75-.75z"
                                  clip-rule="evenodd"/>
                        </svg>
                            </span>
                        <span class="block truncate px-1">{{ $this->selectedOrder()['name'] }}</span>
                        <span class="pointer-events-none">
        <svg class="-mr-1 h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor"
             aria-hidden="true">
                        <path fill-rule="evenodd"
                              d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                              clip-rule="evenodd"/>
        </svg>
      </span>
                    </button>

                    <ul class="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm"
                        tabindex="-1" role="listbox" aria-labelledby="listbox-label"
                        aria-activedescendant="listbox-option-3"
                        x-show="open" x-transition.opacity>

                        @foreach($available_orders as $order_key => $order)
                            <li class="text-gray-900 hover:bg-indigo-600 hover:text-white relative cursor-default select-none py-2 pl-8 pr-4"
                                id="listbox-option-0" role="option"
                                @click="open = false"
                                wire:click="selectOrder('{{ $order_key }}')">
                                <span
                                    class="block truncate {{ $order_key == $selected_order ? 'font-semibold' : 'font-normal' }}">{{ $order['name'] }}</span>

                                @if($order_key == $selected_order)
                                    <span
                                        class="hover:text-white text-indigo-600 absolute inset-y-0 left-0 flex items-center pl-1.5">
          <svg class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path fill-rule="evenodd"
                  d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                  clip-rule="evenodd"/>
          </svg>
        </span>
                                @endif
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>

        <div class="flex  mt-2">
            @foreach($filters as $filter_key => $filter)
                <livewire:actor-dropdown-filter :$filter :$filter_key :key="$filter_key"/>
            @endforeach
        </div>
    </x-page-header>

    <div class="py-4">
        <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
            @if($this->actors->isEmpty())
                No actors found for '{{ $query }}'.
            @else
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg h-auto">
                    <ul role="list"
                        class="mx-auto pt-4 px-4 grid max-w-2xl grid-cols-4 gap-x-6 gap-y-6 sm:grid-cols-6 lg:mx-0 lg:max-w-none md:grid-cols-7 lg:grid-cols-8">
                        @foreach ($this->actors as $actor)
                            <x-actor-card :$actor :key="$actor->id"/>
                        @endforeach
                    </ul>

                    <div class="justify-between border-t border-gray-200 bg-white px-4 py-3 sm:px-6">
                        {{ $this->actors->links() }}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
