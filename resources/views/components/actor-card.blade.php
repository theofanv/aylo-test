@props(['actor'])
<li>
    <a href="{{ route('actors.show',['actor'=>$actor->id]) }}">
        @if($thumbnail = $actor->getThumbnailFor())
            <img class="rounded-2xl object-cover" src="{{ $thumbnail['urls'][0] }}" alt="{{ $actor->name }}"
                 width="{{ $thumbnail['width'] }}" height="{{ $thumbnail['height'] }}">
        @else
            <img class="w-full rounded-2xl object-cover" src="/images/actor-photo.svg">
        @endif
        <h3 class="mt-2 text-lg font-semibold leading-8 tracking-tight text-gray-900">{{ $actor->name }}</h3>
        <div class="flex justify-between">
            <p class="text-base leading-7 text-gray-600">{{ $actor->views }} views</p>
            @if($actor->link)
                <a href="{{ $actor->link }}" class="text-gray-400 hover:text-gray-500">
                    <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 mr-0 mb-0">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 6H5.25A2.25 2.25 0 003 8.25v10.5A2.25 2.25 0 005.25 21h10.5A2.25 2.25 0 0018 18.75V10.5m-10.5 6L21 3m0 0h-5.25M21 3v5.25" />
                    </svg>
                    </div>
                </a>
            @endif
        </div>
    </a>
</li>
