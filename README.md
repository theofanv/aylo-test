Aylo Test
=========

By [Theofanis Vardatsikos](https://www.linkedin.com/in/theofanis-vardatsikos) ([vardtheo@gmail.com](mailto:vardtheo@gmail.com))

A platform that downloads actors from a remote source, stores them in the database and displays them in front-end.

Built with PHP **Laravel**, **Livewire** & **Tailwind**. App is running in Docker using [**Laravel Sail**](https://laravel.com/docs/10.x/sail).

# Installation

Since the app is running in Docker, you should terminate any processes capturing common ports, e.g. 80.

```bash
git clone https://gitlab.com/theofanv/aylo-test.git
cd aylo-test 

# Initial composer install https://laravel.com/docs/10.x/sail#installing-composer-dependencies-for-existing-projects
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs

./vendor/bin/sail up -d
./vendor/bin/sail php artisan actors:sync # Sync actors from the remote source, it might take ~2 minutes.
./vendor/bin/sail npm run dev
```

Finally, browse [http://localhost](http://localhost)

### For local development

```bash
./vendor/bin/sail artisan ide-helper:generate
./vendor/bin/sail artisan ide-helper:meta
./vendor/bin/sail artisan ide-helper:eloquent
./vendor/bin/sail artisan ide-helper:models -N
```

# Notes

1. The command `\App\Console\Commands\SyncActors` runs daily via the scheduler and syncs the actors from the remote json.
2. Source of json file is stored in config `config/actors.php`. 
3. User input (search, ordering, filters) is stored in session for persistence.
4. Search is implemented using MySQL fulltext search. In a realistic scenario I would use MeiliSearch via Laravel Scout.
5. The included unit tests were generated from the generated packages, not manually.
6. Most of the front-end work is located in `\App\Livewire\Actors\IndexActors` and its view.
7. I have added an API Controller `\App\Http\Controllers\API\ActorController` even though it is unused as part of the assessment.

*By the way this does not feel like a proper assessment for a back-end developer.* Nevertheless, it was fun building it.


# Preview

<img src="./screenshots/index-actors-with-sort-and-search.png" alt="Filter Actors"/>
<img src="./screenshots/index-actors-with-filters.png" alt="Search Actors"/>
<img src="./screenshots/show-actor.png" alt="Actor detail page"/>
