<?php

namespace App\Models;

use App\Models\Traits\ModelCaching;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class Actor extends Model
{
    use HasFactory;
    use SoftDeletes;
    use ModelCaching;

    protected $fillable = [
        'attributes',
        'hairColor', 'ethnicity', 'tattoos', 'piercings', 'breastSize', 'breastType', 'gender', 'orientation', 'age', 'birthYear',
        'stats',
        'subscriptions', 'monthlySearches', 'views', 'videosCount', 'premiumVideosCount', 'whiteLabelVideoCount', 'rank', 'rankPremium', 'rankWl',
        'name',
        'license',
        'wlStatus',
        'aliases',
        'link',
        'thumbnails',
    ];

    protected $casts = [
        'tattoos'    => 'bool',
        'piercings'  => 'bool',
        'thumbnails' => 'array',
    ];

    protected $attributes = [
        'thumbnails' => '{}',
    ];

    public function getAgeAttribute(): int
    {
        return now()->year - $this->birthYear;
    }

    public function setAgeAttribute(int $age): int
    {
        return $this->birthYear = now()->year - $age;
    }

    public function aliases(): Attribute
    {
        return Attribute::make(
            get: fn(string $aliases): array => explode(',', $aliases),
            set: fn(array|string $aliases): string => implode(',', (array)$aliases)
        );
    }

    public function setAttributesAttribute(array $attributes): static
    {
        return $this->fill($attributes);
    }

    public function setStatsAttribute(array $stats): static
    {
        return $this->fill($stats);
    }

    public function getThumbnailFor(string $device = 'pc'): ?array
    {
        return Arr::first($this->thumbnails, fn($attrs) => $attrs['type'] == $device)
            ?? Arr::first($this->thumbnails);
    }
}
