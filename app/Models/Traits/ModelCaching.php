<?php

namespace App\Models\Traits;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

/**
 * @mixin Model
 */
trait ModelCaching
{

    protected static function bootModelCaching()
    {
        $clearCacheCallback = fn() => static::clearCache();

        static::saved($clearCacheCallback);
        static::deleted($clearCacheCallback);
        if (in_array(SoftDeletes::class, class_uses_recursive(static::class), true)) {
            static::restored($clearCacheCallback);
        }
    }

    public static function cache(string $key, Closure $callback)
    {
        return Cache::tags(static::class)->remember($key, config('cache.duration', 3600 * 24), $callback);
    }

    public static function clearCache()
    {
        Cache::tags(static::class)->flush();
    }

}
