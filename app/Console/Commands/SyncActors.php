<?php

namespace App\Console\Commands;

use App\Services\ActorService;
use App\Models\Actor;
use Illuminate\Console\Command;
use Illuminate\Support\Benchmark;

class SyncActors extends Command
{
    protected $signature = 'actors:sync';

    protected $description = 'Sync database with actors from remote source.';

    public function handle()
    {
        $this->info("Syncing actors from source...");

        $duration = Benchmark::measure(app(ActorService::class)->updateFromSource(...));
        $duration = intval($duration / 1000);

        $this->info("Imported " . Actor::count() . " actors ($duration s)");

        return 0;
    }
}
