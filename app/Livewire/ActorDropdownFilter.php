<?php

namespace App\Livewire;

use Livewire\Attributes\Modelable;
use Livewire\Component;

class ActorDropdownFilter extends Component
{
    public array $filter;
    public string $filter_key;

    public function selectFilter(string $key)
    {
        $this->filter['selected'] = $key;
        $this->dispatch('update-filter', filter: $this->filter_key, data: ['selected' => $this->filter['selected']]);
    }

    public function render()
    {
        return view('components.actor-dropdown-filter');
    }
}
