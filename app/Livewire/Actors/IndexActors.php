<?php

namespace App\Livewire\Actors;

use App\Models\Actor;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Livewire\Attributes\Computed;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithPagination;

/**
 * @property-read Paginator|Collection<Actor> $actors
 */
class IndexActors extends Component
{
    use WithPagination;

    public ?string $query = null;
    public string $selected_order = 'monthlySearches';

    public array $available_orders = [
        'rank'            => ['name' => 'Most Popular', 'direction' => 'desc'],
        'views'           => ['name' => 'Most Viewed', 'direction' => 'desc'],
        'monthlySearches' => ['name' => 'Top Trending', 'direction' => 'desc'],
        'subscriptions'   => ['name' => 'Most Subscribed', 'direction' => 'desc'],
        'name'            => ['name' => 'Alphabetical', 'direction' => 'desc'],
        'videosCount'     => ['name' => 'No. of Videos', 'direction' => 'desc'],
        'random'          => ['name' => 'Random', 'direction' => 'desc'],
    ];

    public array $filters = [
        'gender'     => ['name' => 'Gender', 'type' => 'dropdown', 'selected' => null, 'options' => []],
        'ethnicity'  => ['name' => 'Ethnicity', 'type' => 'dropdown', 'selected' => null, 'options' => []],
        'tattoos'    => ['name' => 'Tattoo', 'type' => 'dropdown', 'selected' => null, 'options' => [], 'options_type' => 'boolean'],
        'breastSize' => ['name' => 'Cup Size', 'type' => 'dropdown', 'selected' => null, 'options' => []],
        'piercings'  => ['name' => 'Piercing', 'type' => 'dropdown', 'selected' => null, 'options' => [], 'options_type' => 'boolean'],
        'hairColor'  => ['name' => 'Hair Color', 'type' => 'dropdown', 'selected' => null, 'options' => []],
        'breastType' => ['name' => 'Breast Type', 'type' => 'dropdown', 'selected' => null, 'options' => []],
        //'birthYear'  => ['name' => 'Age Group', 'type' => 'slider', 'selected_min' => null, 'selected_max' => null, 'min' => 18, 'max' => 80],
    ];
    private string $filter_select_all_options = 'all-options';

    public function mount(): void
    {
        $this->fillFilters();
        $this->selected_order = $this->getFromSessionOrCurrent('selected_order');
        $this->query = $this->getFromSessionOrCurrent('query');
    }

    private function getFromSessionOrCurrent(string $key): mixed
    {
        return session(static::class . "-$key", $this->$key);
    }

    private function updateSessionFromCurrent(string $key): void
    {
        session([static::class . "-$key" => $this->$key]);
    }

    private function updateSession(string $key, mixed $value): void
    {
        session([static::class . "-$key" => $value]);
    }

    private function getFromSession(string $key, $default = null): mixed
    {
        return session(static::class . "-$key", $default);
    }

    private function fillFilters(): void
    {
        $translate_option = function (array $filter, $option) {
            if (data_get($filter, 'options_type') == 'boolean') {
                return $option ? 'Yes' : 'No';
            }
            return trans()->has($option)
                ? trans($option)
                : str($option)->snake(' ')->title();
        };

        foreach ($this->filters as $key => &$filter) {
            switch ($filter['type']) {
                case 'dropdown':
                    $filter['options'] = Actor::cache("filter-$key",
                        fn() => [$this->filter_select_all_options => 'All (' . Actor::count() . ')']
                            + Actor::query()
                                ->whereNotNull($key)
                                ->groupBy($key)
                                ->get([$key, DB::raw('count(*) as results')])
                                ->keyBy($key)
                                ->map(fn(Actor $a) => $translate_option($filter, $a->$key) . " ({$a->results})")
                                ->toArray()
                    );

                    $filter = $this->getFromSession("filter-$key", ['selected' => $this->filter_select_all_options]) + $filter;
                    break;
                case 'slider':
                    $filter = $this->getFromSession("filter-$key", ['selected_min' => null, 'selected_max' => null]) + $filter;
                    break;
            }
        }
    }

    #[On('update-filter')]
    public function updateFilter(string $filter, array $data): void
    {
        $this->filters[$filter] = $data + $this->filters[$filter];
        $this->updateSession("filter-$filter", $data);
        $this->search();
    }

    public function updatedQuery(): void
    {
        $this->updateSessionFromCurrent('query');
        $this->search();
    }

    /**
     * When user clicks the search button.
     */
    public function search(): void
    {
        $this->resetPage();
    }

    /**
     * When users changes ordering.
     */
    public function selectOrder($order_key): void
    {
        $this->selected_order = $order_key;
        $this->updateSessionFromCurrent('selected_order');
    }

    public function render()
    {
        return view('actors.index');
    }

    #[Computed]
    public function selectedOrder(): array
    {
        return $this->available_orders[$this->selected_order];
    }

    #[Computed]
    public function actors(): Paginator
    {
        return Actor::query()
            ->when($this->query, fn(Builder $q, $s) => $q->whereFullText(['name', 'aliases'], $s))
            ->when($this->selected_order != 'random', fn(Builder $q) => $q->orderBy($this->selected_order, $this->selectedOrder()['direction']))
            ->when($this->selected_order == 'random', fn(Builder $q, $s) => $q->inRandomOrder())
            ->tap(function (Builder $q) {
                foreach ($this->filters as $key => $filter) {
                    if (!empty($filter['selected']) && $filter['selected'] != $this->filter_select_all_options) {
                        $q->where($key, $filter['selected']);
                        continue;
                    }

                    if (!empty($filter['min_selected'])) {
                        $q->where($key, '>=', $filter['min_selected']);
                    }
                    if (!empty($filter['max_selected'])) {
                        $q->where($key, '<=', $filter['max_selected']);
                    }
                }
            })
            ->paginate(40);
    }

}
