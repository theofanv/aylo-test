<?php

namespace App\Livewire\Actors;

use App\Models\Actor;
use Livewire\Component;

class ShowActor extends Component
{
    public Actor $actor;

    public function render()
    {
        return view('actors.show');
    }
}
