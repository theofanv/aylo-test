<?php

namespace App\Http\Resources;

use App\Models\Actor;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property Actor $resource
 * @mixin Actor
 */
class ActorResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        // Simple implementation for the assessment.
        return $this->resource->append('age')->getAttributes();
    }
}
