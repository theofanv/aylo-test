<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ActorResource;
use App\Models\Actor;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    public function __construct()
    {
        // Example protection of endpoints with authentication.
        // $this->middleware('auth:sanctum')->only('store', 'update', 'destroy');
    }

    public function index()
    {
        $this->authorize('viewAny', Actor::class);
        return ActorResource::collection(Actor::all());
    }

    public function store(Request $request)
    {
        $this->authorize('create', Actor::class);
        $request->validate([
            // Simple validation for the assessment.
            'attributes' => 'array',
            'name'       => ['required', 'string'],
            'license'    => ['required', 'string'],
            'link'       => ['nullable', 'url'],
        ]);

        $actor = Actor::make($data = $request->validated());
        $actor->save();

        return ActorResource::make($actor);
    }

    public function show(Actor $actor)
    {
        $this->authorize('view', $actor);
        return ActorResource::make($actor);
    }

    public function update(Request $request, Actor $actor)
    {
        $this->authorize('update', $actor);
        $request->validate([
            // Simple validation for the assessment.
            'attributes' => 'array',
            'name'       => ['required', 'string'],
            'license'    => ['required', 'string'],
            'link'       => ['nullable', 'url'],
        ]);

        $actor->fill($data = $request->validated());

        return ActorResource::make($actor);
    }

    public function destroy(Actor $actor)
    {
        $this->authorize('delete', $actor);
        $actor->delete();

        return ActorResource::make($actor); // Contains deletion info.
    }
}
