<?php

namespace App\Policies;

use App\Models\Actor;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ActorPolicy
{
    use HandlesAuthorization;

    // In real world scenario all these methods would contain business logic.

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Actor $actor): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Actor $actor): bool
    {
        return true;
    }

    public function delete(User $user, Actor $actor): bool
    {
        return true;
    }

    public function restore(User $user, Actor $actor): bool
    {
        return true;
    }

    public function forceDelete(User $user, Actor $actor): bool
    {
        return true;
    }
}
