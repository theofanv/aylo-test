<?php

namespace App\Providers;

use App\Policies\ActorPolicy;
use App\Models\Actor;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        Actor::class => ActorPolicy::class,
    ];

    public function boot(): void
    {
        //
    }
}
