<?php

namespace App\Services;

use App\Models\Actor;
use Illuminate\Database\Eloquent\Collection;

class ActorService
{
    /**
     * @return void
     */
    public function updateFromSource(): void
    {
        $data = file_get_contents(config('actors.source'));
        $data = json_decode($data, true, flags: JSON_THROW_ON_ERROR);
        $actors_data = collect($data['items']);
        Actor::whereKeyNot($actors_data->pluck('id'))->delete(); // Delete obsolete actors.

        // Ideally we need a code other than 'id', to bind with an external system.
        // But now for simplicity 'id' will be used.
        /** @var Collection<Actor> $existing_actors */
        $existing_actors = Actor::withTrashed()->find($actors_data->pluck('id'))->keyBy->id;

        // Cannot use upsert (mass insert/update) because of data transformation.
        $actors_data->each(function (array $actor_data) use ($existing_actors) {
            $actor = ($existing_actors->get($actor_data['id']) ?? new Actor())
                ->fill($actor_data)
                ->setAttribute('id', $actor_data['id']); // Force fill 'id' to use as mapping with third-party system.

            if ($actor->trashed())
                $actor->restore();
            $actor->save(); // No action when restored.
        });
    }

}
