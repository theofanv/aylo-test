<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Actor>
 */
class ActorFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name'    => $this->faker->name,
            'license' => 'REGULAR',
        ];
    }
}
