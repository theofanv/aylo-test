<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('actors', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();

            // Attribute
            $table->string('hairColor', 50)->nullable()->index();
            $table->string('ethnicity', 50)->nullable()->index();
            $table->boolean('tattoos')->nullable()->index();
            $table->boolean('piercings')->nullable()->index();
            $table->integer('breastSize')->nullable()->index();
            $table->string('breastType', 10)->nullable()->index();
            $table->string('gender', 50)->nullable()->index();
            $table->string('orientation', 50)->nullable()->index();
            $table->integer('birthYear')->nullable()->index();

            // Stats
            $table->integer('subscriptions')->default(0)->index();
            $table->integer('monthlySearches')->default(0)->index();
            $table->integer('views')->default(0)->index();
            $table->integer('videosCount')->default(0)->index();
            $table->integer('premiumVideosCount')->default(0)->index();
            $table->integer('whiteLabelVideoCount')->default(0)->index();
            $table->integer('rank')->default(0)->index();
            $table->integer('rankPremium')->default(0)->index();
            $table->integer('rankWl')->default(0)->index();

            $table->string('license');
            $table->string('wlStatus'); // Don't know what this is.
            $table->text('aliases')->nullable();
            $table->string('link')->nullable();
            $table->json('thumbnails'); // This could be a separate table in a real application.
            $table->timestamps();
            $table->softDeletes();
            $table->fullText(['name', 'aliases']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('actors');
    }
};
