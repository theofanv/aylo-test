<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', \App\Livewire\Actors\IndexActors::class)->name('home');

Route::prefix('actors')->name('actors.')->group(function (){
    Route::get('/', \App\Livewire\Actors\IndexActors::class)->name('index');
    Route::get('/{actor}', \App\Livewire\Actors\ShowActor::class)->name('show');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::view('/dashboard', 'dashboard')->name('dashboard');
});
